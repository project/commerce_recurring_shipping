### Configuration

Go to Commerce -> Subscriptions -> Settings and select subscription types that you want to be shippable.
Also make sure the order types support shipping and the product variations (purchasable entity) also is
shippable.

After enabling the shipping for subscription type there will be 2 more fields in subscription bundle and 
also all new recurring orders will get shipments and shipping adjustments.
